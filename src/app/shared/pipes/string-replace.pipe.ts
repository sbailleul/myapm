import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'StringReplace'
})
export class StringReplacePipe implements PipeTransform {

  transform(value: string, stringToReplace: string, replaceString: string): string {
      return value.replace(stringToReplace, replaceString);
  }

}
