import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {StarComponent} from './star-component/star.component';
import {StringReplacePipe} from './pipes/string-replace.pipe';



@NgModule({
  declarations: [
    StarComponent,
    StringReplacePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    StarComponent,
    StringReplacePipe
  ]
})
export class SharedModule { }
