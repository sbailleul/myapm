import { NgModule } from '@angular/core';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductDetailGuard} from './product-detail.guard';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import {RouterModule} from '@angular/router';

const routes = [
  {path: 'products', component: ProductListComponent},
  {path: 'products/:id', canActivate: [ProductDetailGuard], component: ProductDetailComponent},
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
