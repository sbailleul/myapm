import { Component, OnInit } from '@angular/core';
import {IProduct} from '../IProduct';
import {ProductService} from '../product.service';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {


  pageTitle = 'Product List';
  imgWidth = 50;
  imgMargin = 2;
  showImg = false;
  filteredProducts: IProduct[];
  errorMessage: string;

  _listFilter: string;

  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this._listFilter)  : this._products;
  }

  _products: IProduct[];

  constructor(private _productService: ProductService) {
  }

  ngOnInit() {
    this._productService.getProducts().subscribe(products => {
      this._products = products;
      this.filteredProducts = this._products;
    },
      error => this.errorMessage = error);
  }

  performFilter(filterBy: string): IProduct[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this._products.filter(product => product
      .productName
      .toLocaleLowerCase()
      .indexOf(filterBy) !== -1
    );
  }

  toggleImg(): void {
    this.showImg = !this.showImg;
  }

  onNotify(message: string) {
    this.pageTitle = message;
  }
}
